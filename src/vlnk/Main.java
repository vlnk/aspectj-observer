package vlnk;

import vlnk.publisher.PublisherArt;
import vlnk.publisher.PublisherSport;
import vlnk.subscriber.ASubscriber;
import vlnk.subscriber.InternetUser;

public class Main {
    public static void main(String[] args) {
        /*
        List<Publisher> subjects = new ArrayList<>();
        
        for (int i = 0; i < 3; ++i) {
            subjects.add(new Publisher());
        }

        for (Publisher subject : subjects) {
            subject.run();
        }

        for (Publisher subject : subjects) {
            subject.run();
        }
        */

        InternetUser subscriber1 = new InternetUser();
        InternetUser subscriber2 = new InternetUser();

        PublisherArt publisher1 = new PublisherArt();
        PublisherSport publisher2 = new PublisherSport();

        //Publisher publisher2 = new Publisher();

        subscriber1.subscribe(publisher1);
        subscriber1.subscribe(publisher2);
        subscriber2.subscribe(publisher2);

        publisher1.write();
        publisher2.write();

        subscriber1.unsubscribe(publisher1);
        publisher1.write();
    }
}
