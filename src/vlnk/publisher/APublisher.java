package vlnk.publisher;

public abstract class APublisher {
    public static final String PUBLISHER_COLOR = "\u001B[34m";
    public static final String ANSI_RESET = "\u001B[0m";

    public void write(String article) {
        String log = PUBLISHER_COLOR + '(' + this + ')' + ANSI_RESET;
        String talk = " I wrote an article about ";
        System.out.println(log + talk + article);
    }

    @Override
    public String toString() { return this.getClass().getSimpleName(); }
}
