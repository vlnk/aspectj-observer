package vlnk.publisher;

import vlnk.ISubject;

public class PublisherSport extends APublisher implements ISubject<String>{
    @Override
    public void notify(String message) {  }

    @Override
    public void write(String article) {
        super.write(article);
        notify(article);
    }

    public void write() { write("Sport"); }
}
