package vlnk;

public interface ISubject<T> {
    void notify(T t);

}
