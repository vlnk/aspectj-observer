package vlnk;

public interface IObserver<T> {
    void update(T t, ISubject from);
    void attach(ISubject subject);
    void detach(ISubject subject);
}
