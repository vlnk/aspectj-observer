package vlnk.subscriber;

import vlnk.ISubject;

public abstract class ASubscriber {
    public static final String SUBSCRIBER_COLOR = "\u001B[31m";
    public static final String ANSI_RESET = "\u001B[0m";
    protected static int ID = 0;

    public ASubscriber() { ID++; }

    public abstract void subscribe(ISubject subject);
    public abstract void unsubscribe(ISubject subject);

    public abstract void read(String article, ISubject from);

    @Override
    public String toString() { return this.getClass().getSimpleName(); }
}
