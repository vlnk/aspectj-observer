package vlnk.subscriber;

import vlnk.IObserver;
import vlnk.ISubject;

public class InternetUser extends ASubscriber implements IObserver<String>{
    private final int id;

    public InternetUser() {
        super();
        id = ASubscriber.ID;
    }

    public int getId() { return id; }

    public void subscribe(ISubject subject) {
        attach(subject);
    }

    public void unsubscribe(ISubject subject) {
        detach(subject);
    }

    @Override
    public void read(String article, ISubject from) {
        String log = SUBSCRIBER_COLOR + '(' + this + ')' + ANSI_RESET;
        String talk = " I read : " + article + " from " + from;
        System.out.println(log + talk);
    }

    @Override
    public void attach(ISubject subject) {}

    @Override
    public void detach(ISubject subject) {}

    @Override
    public void update(String message, ISubject from) {
        read(message, from);
    }

    @Override
    public String toString() { return this.getClass().getSimpleName() + id; }
}
