package vlnk.aspects;

import vlnk.IObserver;
import vlnk.ISubject;

import java.util.*;

public aspect ObserverProtocol {
    public static final String LOG_COLOR = "\u001B[34m";
    public static final String ANSI_RESET = "\u001B[0m";

    public Dictionary<ISubject, List<IObserver>> observersPerSubject = new Hashtable<>();

    public List<IObserver> getObservers(ISubject subject) {
        return observersPerSubject.get(subject);
    }

    public void addObserver(ISubject subject, IObserver observer) {
        List<IObserver> observersForSubject = getObservers(subject);

        if (!observersForSubject.contains(observer)) {
            observersForSubject.add(observer);
        }
    }

    public void removeObserver(ISubject subject, IObserver observer) {
        List<IObserver> observersForSubject = getObservers(subject);
        observersForSubject.remove(observer);
    }

    public void updateObserver(ISubject subject, String message) {
        if (getObservers(subject).size() == 0)
            printLog("Any assigment has made with " + subject);
        else
            for (IObserver observer : getObservers(subject)) {
                observer.update(message, subject);
                printLog(observer + " has been updated by " + subject);
            }
    }

    public void printLog(String log) {
        System.out.println("(log) " + log);
    }

    pointcut changement(ISubject subject, String message):
        execution(* *.notify(String)) && target(subject) && args(message);

    after(ISubject subject, String message): changement(subject, message) {
        updateObserver(subject, message);
    }

    pointcut assignment(IObserver observer, ISubject subject):
        execution(* *.attach(ISubject)) && target(observer) && args(subject);

    before(IObserver observer, ISubject subject): assignment(observer, subject) {
        addObserver(subject, observer);
        printLog(observer + " is attached to " + subject);
    }

    pointcut unassignment(IObserver observer, ISubject subject):
            execution(* *.detach(ISubject)) && target(observer) && args(subject);

    before(IObserver observer, ISubject subject): unassignment(observer, subject) {
        removeObserver(subject, observer);
        printLog(observer + " is detached from " + subject);
    }

    pointcut creation(ISubject subject): execution(*.new(..)) && target(subject);
    after(ISubject subject): creation(subject) {
        observersPerSubject.put(subject, new ArrayList<>());
        printLog(subject + ": well instanciated.");
    }
}
